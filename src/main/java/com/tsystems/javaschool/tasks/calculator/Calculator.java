package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {
        // TODO: Implement the logic here
        int operatorIndex;
        double result;
        String resultString;

        if(statement == null || statement.matches(".*[-+*/.][-+*/.]+.*") || !statement.matches("[[0-9]-/*+.()]+")) {
            return null;
        }

        while (statement.contains("(")) {
            int left = statement.indexOf("(");
            if (statement.substring(left).contains(")")) {
                statement = evaluateParentheses(statement);
            } else return null;

        }

        if (statement.contains("(") || statement.contains(")")) {
            return null;
        }

        while (statement.contains("/")) {
            operatorIndex = statement.indexOf("/");
            statement = evaluatePart(statement, operatorIndex);
        }

        while (statement.contains("*")) {
            operatorIndex = statement.indexOf("*");
            statement = evaluatePart(statement, operatorIndex);
        }

        while (statement.contains("-")) {
            operatorIndex = statement.lastIndexOf("-");
            if (operatorIndex != 0) statement = evaluatePart(statement, operatorIndex);
            else break;
        }

        while (statement.contains("+")) {
            operatorIndex = statement.indexOf("+");
            statement = evaluatePart(statement, operatorIndex);
        }

        result = Double.parseDouble(statement);
        if (result == Double.NEGATIVE_INFINITY | result == Double.POSITIVE_INFINITY) return null;

        DecimalFormat df = new DecimalFormat("#.####");
        resultString = df.format(result);

        return resultString;
    }

    private String evaluateParentheses(String statement){
        int left = statement.lastIndexOf("(");
        int right = left + statement.substring(left).indexOf(")");
        String tempStatement = statement.substring(left+1, right);
        String result = evaluate(tempStatement);
        statement = statement.substring(0, left) + result + statement.substring(right+1);
        statement = statement.replace("--", "+");
        statement = statement.replace("+-", "-");
        return statement;
    }


    private String evaluatePart(String statement, int index) throws NumberFormatException{
        int[] borders = findBorders(statement, index);
        String leftOperand = statement.substring(borders[0], index);
        String rightOperand = statement.substring(index+1, borders[1]+1);
        Double left = Double.parseDouble(leftOperand);
        Double right = Double.parseDouble(rightOperand);
        Double result;
        switch (Character.toString(statement.charAt(index))){
            case "*":
                result = left*right;
                break;
            case "/":
                result = left/right;
                break;
            case "+":
                result = left+right;
                break;
            case "-":
                result = left-right;
                break;
            default:
                result = null;
                break;
        }

        String resultString = result.toString();
        statement = statement.substring(0, borders[0]) + resultString + statement.substring(borders[1]+1);
        return statement;
    }


    private int[] findBorders(String statement, int index) {
        int[] borders = new int[2];
        borders[1] = statement.length()-1;

        int i = 1;
        while((index - i)>0 && (!Character.toString(statement.charAt(index-i)).matches("[-*/+]"))) i++;
        if (index - i > 0) {
            if (Character.toString(statement.charAt(index-i)).equals("-")) borders[0]=index-i;
            else borders[0] = index - i + 1;
        }

        i = 2;
        while((index + i < statement.length()) && !Character.toString(statement.charAt(index+i)).matches("[-*/+]")) i++;
        if (index + i <= statement.length()) borders[1] = index + i - 1;
        return borders;
    }
}
