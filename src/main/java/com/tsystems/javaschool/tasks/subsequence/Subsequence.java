package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if (x == null | y == null) throw new IllegalArgumentException();

        /*if (x == null) return true;
        if (y == null) return false;*/

        int xLength = x.size();
        int yLength = y.size();
        boolean[] tempFlags = new boolean[xLength];

        int j = 0;
        for(Object temp: x){
            for(; j<yLength; j++){
                if (temp.equals(y.get(j))){
                    tempFlags[x.indexOf(temp)] = true;
                    break;
                }
            }
        }

        for (int i=0; i<xLength; i++){
            if (!tempFlags[i]) return false;
        }

        return true;
    }
}
