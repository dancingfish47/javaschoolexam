package com.tsystems.javaschool.tasks.pyramid;

import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here

        int length = inputNumbers.size();
        if (length < 1) throw new CannotBuildPyramidException();
        int rowCount = 0;
        int columnCount;

        int tempSum = 0;
        int z = 1;
        while (tempSum!=length){
            tempSum+=z;
            if(rowCount==Integer.MAX_VALUE) throw new CannotBuildPyramidException();
            else rowCount++;
            z++;
            if(tempSum>length) throw new CannotBuildPyramidException();
        }

        columnCount = rowCount*2 - 1;

        inputNumbers.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
               try{
                   return o1.compareTo(o2);
               } catch (NullPointerException e){
                   throw new CannotBuildPyramidException();
               }
            }
        });

        int[][] pyramid = new int[rowCount][columnCount];

        int c = 0;
        int lastIndex = length-1;
        while(c<rowCount){
            for(int a = rowCount - 1; a>=0; a--){
                for(int b = columnCount-1-c; b>= c; b=b-2){
                    pyramid[a][b]=inputNumbers.get(lastIndex);
                    lastIndex--;
                }
                c++;
            }
        }
        return pyramid;
    }



}
